// ConsoleApplication10.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <conio.h>
using namespace std;

bool getInput(char *c);


int main(void)
{
	char key = ' ';

	while (key != 'q')
	{
		while (!getInput(&key))
		{
			// Update your timer here
		}

		// Update Program Logic Here
		cout << "You Pressed: " << key << endl;
	}

	cout << "You Pressed q, End Of Program" << endl;
	return 0;
}

// Get Input
bool getInput(char *c)
{
	if (_kbhit())
	{
		*c = getchar();
		return true; // Key Was Hit
	}
	return false; // No keys were pressed
}